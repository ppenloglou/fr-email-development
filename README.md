# fr-email-development

_**fr-email-development**_ is a project that simplifies the creation of HTML emails using the [MJML framework](https://mjml.io/). It provides a templating system that allows developers to efficiently build consistent email templates. This is achieved by calling partial templates (e.g. ```preheader``` or ```header```) and passing parameters to them (e.g. ```button_href='donate.wikimedia.org/'```). The project also performs various post-processing tasks on the final HTML output, such as removing unnecessary CSS classes and beautifying the code.

## Features

- Simplify HTML email creation with MJML coding.
- Utilize a templating system for consistent email templates.
- Pass parameters to partial templates to customize content.
- Perform post-processing on the HTML output to optimize and beautify the code.

## Requirements

To use _**fr-email-development**_, you need to have **Node.js** and **npm** installed. If you haven't yet installed them, follow the steps [outlined here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) relevant to your operating system. As mentioned in the guide, it is recommended to install Node.js and npm via NVM (Node Version Manager).

It's recommended you use a modern code editor such as [Visual Studio Code](https://code.visualstudio.com/) for ease of use but it's up to you and choosing a different one won't hinder you.

## Getting Started

1. Clone this repository to a local directory on your system. If you're using Visual Studio Code, GitLab offers a quick Clone selection via SSH or HTTPS.
2. Navigate to your newly cloned repository in your IDE of choice. The easiest way would be to select 'Open Folder' and select the parent directory ```fr-email-development```.
3. If it's not present already, create a directory named ```templates```. This is where all of your template coding will happen.

## Usage

1. In the parent directory, open a new terminal and run ```npm run watch```. This command listens for changes to ```*.mjml``` files located in the ```templates``` directory. You can have as many nested directories as you want.\
**Don't use whitespaces in directory or file names** or ```npm run watch``` will fail.
2. Create or modify MJML email templates in the `templates` directory.
3. (Optional) Utilize partial templates by calling them in your main templates and passing parameters.
4. Save your changes to your template (e.g. ```English_Email1.mjml```) and the HTML version will be automatically generated in the same directory (e.g. ```English_Email.html```)

## Templates directory (Git Submodule)

It is important to note that there is a second git repository associated with _**fr-email-development**_ and that is [_**fr-email-templates**_](https://gitlab.wikimedia.org/ppenloglou/fr-email-templates). This is done in order to keep the development of _**fr-email-development**_ and the creation of email templates separate. Email developers will be assigned their own git branch within _**fr-email-templates**_ which will look like ```templates-username``` (e.g. ```templates-jdoe```). Using this setup, email developers are able to create/edit templates whilst having a version control system in place for their work. Additionally, they won't be able to accidentally affect anyone else's work.

## Documentation & Wiki for coding

Visit [HTML Email Development](https://wikitech.wikimedia.org/wiki/Fundraising/Development_tools/HTML_Email_Development) on Wikitech.

## Contact

For questions, suggestions, or feedback, you can reach out to me at [ppenloglou@wikimedia.org](mailto:ppenloglou@wikimedia.org) or on Slack [@panos](https://wikimedia.slack.com/team/U049CKDPLRZ).
