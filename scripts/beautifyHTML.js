const { promises: fsPromises } = require('fs');
const beautify = require('js-beautify').html;

const HTMLPATH = process.env.MJMLPATH.replace(".mjml", ".html");

async function beautifyHTML() {
    try {
        const data = await fsPromises.readFile(HTMLPATH, 'utf8');
        const beautifulHTML = beautify(data, { indent_size: 2, space_in_empty_paren: true });
        await fsPromises.writeFile(HTMLPATH, beautifulHTML, "utf8");
        console.log("beautifyHTML.js: Complete.");
    } catch (error) {console.error('Error occurred:', error);}
}

beautifyHTML();
