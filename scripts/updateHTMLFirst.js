// Import necessary modules
const { readFileSync, promises: fsPromises } = require('fs');
const replace = require('replace-in-file');
const path = require('path');

// Get the HTML file path from environment variable and convert to .html
const HTMLPATH = process.env.MJMLPATH.replace(".mjml", ".html");

// Entry point function
updateHTMLFirst();

function updateHTMLFirst() {
    try {
        process.stdout.write("\u001b[32m"); // Set terminal color to green
        // Perform various replacements and cleanups
        replaceFromTo("mj-(?!raw)", "wmf-email-");
        replaceFromTo("wmf-email-column-per-33-333333333333336", "wmf-email-column-per-33");
        replaceFromTo("’", "&#39;");
        replaceFromTo("--><!--", "-->\n<!--");
        removeUnusedClasses();
        console.log("updateHTMLFirst.js: Complete.");
    } catch (error) {
        console.error('Error occurred:', error);
    }
}

// Function to replace text from and to in a given file
function replaceFromTo(from, to) {
    try {
        const options = {
            files: HTMLPATH,
            from: new RegExp(from, 'g'),
            to: to,
        };
        replace.sync(options);
    } catch (error) {
        console.error('Error occurred:', error);
    }
}

// Function to remove unused CSS classes from the HTML file
function removeUnusedClasses() {
    try {
        // Read and extract CSS classes from styles.mjml
        const stylesPath = path.join(__dirname, '..', 'partials', 'utilities', 'styles.mjml');
        const stylesFile = readFileSync(stylesPath, 'utf-8');
        const cssClassRegex = /\.([a-zA-Z0-9s-]+)\s/g; // Regular expression to match class names
        let cssClasses = [];
        let match;
        while ((match = cssClassRegex.exec(stylesFile)) !== null) {
            cssClasses.push(match[1]);
        }
        cssClasses = [...new Set(cssClasses)]; // Remove duplicates

        // Read HTML file content and extract content after <body> tag
        const htmlFile = readFileSync(HTMLPATH, 'utf-8');
        const bodySubStr = htmlFile.substring(htmlFile.indexOf("<body"));

        // Check and remove unused CSS classes
        cssClasses.forEach(element => {
            if (!checkIfContains(bodySubStr, element)) {
                const regEx = `\\.${element}.*[a-zA-Z]* {[^]*?}`;
                const options = {
                    files: HTMLPATH,
                    from: new RegExp(regEx, "g"),
                    to: "",
                };
                replace.sync(options);
            }
        });
    } catch (error) {
        console.error('Error occurred:', error);
    }
}

// Function to check if a given text contains a specific string
function checkIfContains(text, str) {
    return text.includes(str);
}
