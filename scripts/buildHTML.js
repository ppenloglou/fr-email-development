// Requiring necessary modules and libraries
const mjml2html = require('mjml');
const MJMLPATH = process.env.MJMLPATH;
const mjmlOptions = {
  keepComments: true
};

const { readFileSync, writeFileSync, promises: fsPromises, fileSystem } = require('fs');
const path = require('path');
const fs = require('fs');
const partialsDir = path.join(__dirname, '..', 'partials');
const Handlebars = require("handlebars");

// Function to recursively get files in a directory
function getFilesRecursively(Directory) {
  fs.readdirSync(Directory).forEach(File => {
    const partialFilePath = path.join(Directory, File);
    if (fs.statSync(partialFilePath).isDirectory()) return getFilesRecursively(partialFilePath);
    else registerPartial(partialFilePath);
  });
}

// Function to register a Handlebars partial
function registerPartial(filePath) {
  partialName = filePath.substring(filePath.lastIndexOf('/') + 1, filePath.indexOf('.'));
  inputPartial = readFileSync(filePath, 'utf-8');
  Handlebars.registerPartial(partialName, inputPartial);
}

// Handlebars Helper functions
Handlebars.registerHelper('helperMissing', function (/* dynamic arguments */) {
  var options = arguments[arguments.length - 1];
  var args = Array.prototype.slice.call(arguments, 0, arguments.length - 1)
  return new Handlebars.SafeString(`{{${options.name}}}`);
});

Handlebars.registerHelper('default', function (value, safeValue) {
  var out = value || safeValue;
  return new Handlebars.SafeString(out);
});

Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
  if (arg1 instanceof Handlebars.SafeString) arg1 = arg1.toString();
  if (arg2 instanceof Handlebars.SafeString) arg2 = arg2.toString();
  if (arg1 === arg2) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});

Handlebars.registerHelper('def', function (varName, value, options) {
  if (typeof value === 'function') value = value().toString();
  this[varName] = value;
});

Handlebars.registerHelper('obj', function (options) {
  const hash = options.hash;
  return hash;
});

Handlebars.registerHelper('array', function (options) {
  const items = Array.from(arguments).slice(0, -1);
  return items;
});

Handlebars.registerHelper('objList', function (options) {
  const hash = options.hash;
  return hash;
});

// Function to compile MJML into HTML
function buildHTML() {
  // Print console output with colored terminal text
  process.stdout.write("\u001b[32m"); // Terminal Coloring
  console.log("buildHTML.js: Building HTML from:\u001b[33m " + MJMLPATH.substring(MJMLPATH.lastIndexOf('/') + 1));
  
  // Registering partials for handlebars
  try {getFilesRecursively(partialsDir);}
  catch (err) {console.log(err);}

  // Define paths
  var mjmlSource = MJMLPATH;
  var mjmlTemp = path.join(`templates`, `temp.mjml`);
  var htmlSource = MJMLPATH.replace(".mjml", ".html");

  // Read MJML source file and compile it using Handlebars
  inputFile = readFileSync(mjmlSource, 'utf-8');
  var template = Handlebars.compile(inputFile);
  var context = {};
  var newMJML = template(context);

  // Write compiled MJML to temporary file
  writeFileSync(mjmlTemp, newMJML, "utf8");

  // Read the temporary MJML file, convert to HTML using mjml2html
  mjmlInputFile = readFileSync(mjmlTemp, 'utf-8');
  const mjml2htmlOutput = mjml2html(mjmlInputFile, mjmlOptions);

  // Write the final HTML output to file
  writeFileSync(htmlSource, mjml2htmlOutput.html, "utf8");
}

// Build HTML using the defined function
buildHTML();
