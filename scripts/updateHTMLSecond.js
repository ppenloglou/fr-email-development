const { readFileSync, writeFileSync, promises: fsPromises } = require('fs');
const removeBlankLines = require('remove-blank-lines');
const HTMLPATH = process.env.MJMLPATH.replace(".mjml",".html");
// Start program

inputFile = readFileSync(HTMLPATH, 'utf-8');
newHTML = removeBlankLines(inputFile);
writeFileSync(HTMLPATH, newHTML, "utf8");

removeEmptyStyleTag();

// Looks for 'style=""' in body and removes it.
function removeEmptyStyleTag() {
	try {
		inputFile = readFileSync(HTMLPATH, 'utf-8');
		var regEx = `style=""`;
		regEx = new RegExp(regEx, "g");
		let newHTML = inputFile.replace(regEx, '');
		writeFileSync(HTMLPATH, newHTML, "utf8");
	}
	catch (error) {console.error('Error occurred:', error);}
}

// Terminal logging.
console.log("updateHTMLSecond.js: Complete.");
process.stdout.write("\u001b[0mHTML produced at:" ); // Terminal Coloring Reset
console.log("\u001b[33m "+HTMLPATH+"\u001b[0m \n");
